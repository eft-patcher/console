﻿using BsDiff;
using FastRsync.Delta;
using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;
using System.IO.Compression;
using System.Text.Json;

namespace EFT_Patcher
{
    [Command(Name = "EFT-Patcher", Description = "Unofficial EFT patcher/updater")]
    [HelpOption]
    internal class Program
    {
        [Required]
        [Option("-u|--update-file <UPDATE_FILE>", "Path to .update file", CommandOptionType.SingleValue)]
        [FileExists]
        private string UpdateFile { get; } = "";

        [Required]
        [Option("-e|--eft-directory <EFT_DIR>", "Path to EFT directory to upgrade", CommandOptionType.SingleValue)]
        [DirectoryExists]
        private string TargetEFTDirectory { get; } = "";

        [Option("--spt", "Set this to upgrade SPT directory (not live)", CommandOptionType.NoValue)]
        private bool IsSPT { get; } = false;

        [Option("-v|--verbose", "Log success messages and detailed errors", CommandOptionType.NoValue)]
        private bool ShouldLogVerbose { get; } = false;

        private static int Main(string[] args) => CommandLineApplication.Execute<Program>(args);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "Called by CommandLineUtils")]
        private void OnExecute()
        {
            var has_error = false;
            var tmp_dir = ".eft-patcher";
            var update_files_dir = Path.Combine(tmp_dir, "update");
            var patched_files_dir = Path.Combine(tmp_dir, "patched");

            if (Directory.Exists(tmp_dir))
            {
                Directory.Delete(tmp_dir, true);
            }

            Directory.CreateDirectory(update_files_dir);
            Directory.CreateDirectory(patched_files_dir);

            try
            {
                ZipFile.ExtractToDirectory(UpdateFile, update_files_dir);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: Cannot extract .update file!");
                Console.WriteLine(ex.ToString());
                Environment.Exit(1);
            }

            var updateInfoFilePath = Path.Combine(update_files_dir, "UpdateInfo");
            UpdateInfo? updateInfo = null;

            if (File.Exists(updateInfoFilePath))
            {
                try
                {
                    updateInfo = JsonSerializer.Deserialize<UpdateInfo>(File.ReadAllText(updateInfoFilePath));

                    if (updateInfo?.metadataVersion != 2)
                    {
                        throw new Exception("Not (yet) supported update metadata version! Please report");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR: Cannot parse UpdateInfo file!");
                    Console.WriteLine(ex.ToString());
                    Environment.Exit(1);
                }
            }
            else
            {
                Console.WriteLine("ERROR: UpdateInfo file not found!");
                Environment.Exit(1);
            }

            var files = updateInfo?.files ?? Array.Empty<FileUpdateInfo>();

            Parallel.ForEach(files, fileUpdateInfo =>
            {
                var baseFilePath = fileUpdateInfo.path;
                var updateFilePath = Path.Combine(update_files_dir, baseFilePath);
                var originalFilePath = Path.Combine(TargetEFTDirectory, baseFilePath);
                var patchedFilePath = Path.Combine(patched_files_dir, baseFilePath);

                Directory.CreateDirectory(Path.GetDirectoryName(patchedFilePath)!);

                switch (fileUpdateInfo.state)
                {
                    case FileState.Patched:
                        // SPT modifies game file. *Right now* they back up the original file with .bak
                        // We will use this file if it exists
                        if (IsSPT && File.Exists(originalFilePath + ".bak"))
                        {
                            File.Move(originalFilePath + ".bak", originalFilePath, true);
                        }

                        var patchFilePath = updateFilePath + ".patch";

                        if (File.Exists(originalFilePath))
                        {
                            switch (fileUpdateInfo.patchAlgorithmId)
                            {
                                case PatchAlgorithm.Bsdiff:
                                    try
                                    {
                                        BsPatchFile(originalFilePath, patchedFilePath, patchFilePath);

                                        if (ShouldLogVerbose)
                                            Console.WriteLine($"Patching {baseFilePath} - OK");
                                    }
                                    catch (Exception ex)
                                    {
                                        has_error = true;
                                        Console.WriteLine($"Patching {baseFilePath} - FAILED - Cannot patch BSDIFF file. The game file has been modified or you are using the wrong version.");

                                        if (ShouldLogVerbose)
                                            Console.WriteLine(ex.ToString());
                                    }
                                    break;
                                case PatchAlgorithm.FastRsync:
                                    try
                                    {
                                        FastRsyncPatchFile(originalFilePath, patchedFilePath, patchFilePath);

                                        if (ShouldLogVerbose)
                                            Console.WriteLine($"Patching {baseFilePath} - OK");
                                    }
                                    catch (Exception ex)
                                    {
                                        has_error = true;
                                        Console.WriteLine($"Patching {baseFilePath} - FAILED - Cannot patch FastRsync file. The game file has been modified or you are using the wrong version.");

                                        if (ShouldLogVerbose)
                                            Console.WriteLine(ex.ToString());
                                    }
                                    break;
                                default:
                                    has_error = true;
                                    Console.WriteLine($"Patching {baseFilePath} - FAILED - Unknown patch algorithm");
                                    break;
                            }
                        }
                        else
                        {
                            // SPT removes unnecessary game files so file not found is normal
                            if (!IsSPT)
                            {
                                has_error = true;
                                Console.WriteLine($"Patching {baseFilePath} - FAILED - Original file not found");
                            }
                        }
                        break;
                    case FileState.Copy:
                        try
                        {
                            if (Path.GetFileName(baseFilePath) != "UpdateInfo")
                            {
                                File.Copy(updateFilePath, patchedFilePath);

                                if (ShouldLogVerbose)
                                    Console.WriteLine($"Copying {baseFilePath} - OK");
                            }
                        }
                        catch
                        {
                            has_error = true;
                            Console.WriteLine($"Copying {baseFilePath} - FAILED - Cannot copy file");
                        }
                        break;
                    case FileState.Delete:
                        break;
                    default:
                        break;
                }
            });

            if (has_error)
            {
                Console.WriteLine("Patching failed. There are errors during patching, no files were updated. Please check the log above.");
            }
            else
            {
                Console.WriteLine("Successfully patched! Copying files to EFT live folder...");

                var files_patched = Directory.GetFiles(patched_files_dir, "*", SearchOption.AllDirectories);

                Parallel.ForEach(files_patched, patchedFilePath =>
                {
                    var baseFilePath = Path.GetRelativePath(patched_files_dir, patchedFilePath);
                    var originalFilePath = Path.Combine(TargetEFTDirectory, baseFilePath);

                    try
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(originalFilePath)!);
                        File.Move(patchedFilePath, originalFilePath, true);
                    }
                    catch
                    {
                        has_error = true;
                        Console.WriteLine($"Failed copying file {baseFilePath}");
                    }
                });

                if (!has_error)
                {
                    Directory.Delete(tmp_dir, true);
                    Console.WriteLine($"Update completed from {updateInfo?.fromVersion} to {updateInfo?.toVersion} with no errors :)");
                }
                else
                {
                    Console.WriteLine("Update completed with errors when copying files!");
                }
            }
        }

        private static void BsPatchFile(string originalFilePath, string destinationFilePath, string patchFilePath)
        {
            using var input = new FileStream(originalFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            using var output = new FileStream(destinationFilePath, FileMode.Create);
            BinaryPatch.Apply(input, () => new FileStream(patchFilePath, FileMode.Open, FileAccess.Read, FileShare.Read), output);
        }

        private static void FastRsyncPatchFile(string originalFilePath, string destinationFilePath, string patchFilePath)
        {
            var patcher = new DeltaApplier
            {
                SkipHashCheck = true
            };

            using var originalFileStream = new FileStream(originalFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            using var patchFileStream = new FileStream(patchFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            using var newFileStream = new FileStream(destinationFilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            patcher.Apply(originalFileStream, new BinaryDeltaReader(patchFileStream, null), newFileStream);
        }
    }
}
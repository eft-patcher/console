﻿namespace EFT_Patcher
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles")]
    internal class UpdateInfo
    {
        public IList<FileUpdateInfo> files { get; set; } = Array.Empty<FileUpdateInfo>();
        public int metadataVersion { get; set; }
        public string fromVersion { get; set; } = "";
        public string toVersion { get; set; } = "";
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles")]
    internal class FileUpdateInfo
    {
        public string path { get; set; } = "";
        public FileState state { get; set; }
        public PatchAlgorithm patchAlgorithmId { get; set; }
    }

    internal enum FileState
    {
        Delete,
        Patched,
        Copy,
    }

    internal enum PatchAlgorithm
    {
        FastRsync,
        Bsdiff
    }
}
